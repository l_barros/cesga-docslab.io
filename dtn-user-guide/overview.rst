.. _intro:

Introduction
============

The Data Transfer Node (DTN) is a server specially configured to enable fast and reliable transfers.

The DTN provides different tools to transfer files from or to CESGA.

Depending on the size of the data that you want to transfer you have different options:

- For small amounts of data (<10GB) you can use scp directly.
- For large amounts of data it is recommended that you use Globus_.

.. _Globus: https://globus.org

In all cases it is recommended that you do the transfer against our DTN server: **dtn.srv.cesga.es**, this will give you a much better network performance. In Globus Online the endpoint of this server is: **cesga#dtn**.

.. note: To transfer large amounts of data we recommend Globus.

.. figure:: _static/screenshots/globus-dtn.png
    :align: center

    Using Globus to transfer files.

If you are going to transfer files from your local computer it is important that you check your local connectivity because it will generally limit the overall speed of the transfer.
