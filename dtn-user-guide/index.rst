.. DTN User Guide documentation master file, created by
   sphinx-quickstart on Thu Apr  4 20:52:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CESGA DTN User Guide
====================

.. toctree::

   overview
   globus
   scp
   aspera
   known_issues
   want_to_know_more
