To create the documentation run:

    make clean
    make html
    make latexpdf

You can use the build script:

    ./build.sh

Finally you can sync the contents with a public web server:

    ./sync_web.sh
