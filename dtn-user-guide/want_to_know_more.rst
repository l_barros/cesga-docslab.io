.. _dtn_want_to_know_more:

Want to know more
=================

For more information you can look at the official documentation of each tool.

- `Globus Howtos`_
- `Globus Documentation`_
- `Aspera CLI Documentation`_.

.. _Globus Howtos: https://docs.globus.org/how-to/
.. _Globus Documentation: https://docs.globus.org/
.. _Aspera CLI Documentation: http://download.asperasoft.com/download/docs/cli/3.7.7/user_linux/webhelp/index.html
