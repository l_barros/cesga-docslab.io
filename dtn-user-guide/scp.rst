.. _scp:

SCP/SFTP
========

Using SCP or SFTP is a useful alternative for small transfers (less than 10GB of data).

Just keep in mind that you will get poorer performance than with Globus and if the connection is not stable you can find issues.

Installation
------------

You just need a local SCP/SFTP client, this is usually already available in Linux and Mac. In case of Windows you can install for example WinSCP_.

.. _WinSCP: https://winscp.net


Usage
-----

Just point your SCP/SFTP client to dtn.srv.cesga.es and then start the transfer.

There is no need to start the VPN to connect to CESGA DTN, but there is also no need to stop it. The transfer speed to the DTN will not be affected by the VPN.

.. note:: The DTN service is configured so it will not be routed through CESGA VPN even if you have it running.
