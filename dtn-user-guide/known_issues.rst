.. _known_issues:

Known Issues
============

tcllib error when using Anaconda
--------------------------------
There is a bug in the tcl version distributed with some Anaconda versions that leads to an error when trying to start the Globus Connect Personal GUI.

The easiest solution is to change the PATH so it does not include the Anaconda directory.


