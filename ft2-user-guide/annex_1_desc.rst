.. _ft2_annex_1_desc:

Annex I: System description
===========================

Nodos Finis Terrae II

Finis Terrae II es un sistema de computación basado en procesadores
Intel Haswell e interconectado mediante red Infiniband con un
rendimiento pico de 328 TFlops y sostenido en Linpack de 213 Tflops.
Está compuesto por 3 tipos de nodos de computación:

306 nodos de cómputo “Thin” cada uno con:

-  2 procesadores Haswell 2680v3, 24 cores
-  128 GB memoria
-  1 disco de 1TB
-  2 conexiones 1GbE
-  1 conexión Infiniband FDR@56Gbps

5 Nodos de cómputo con Aceleradores:

4 nodos con GPUs, cada uno con:

-  2 GPUs NVIDIA Tesla K80 (2 GPUs por cada tarjeta, 4 GPUs por nodo en
   total)
-  2 procesadores Haswell 2680v3, 24 cores
-  128 GB memoria
-  2 discos de 1TB
-  2 conexiones 1GbE
-  1 conexión Infiniband FDR@56Gbps

(Nodos c7257-c7260)

1 nodo con GPU:

-  1 GPUs NVIDIA Tesla V100 PCIe
-  2 procesadores Haswell 2680v3, 24 cores
-  128 GB memoria
-  2 discos de 1TB
-  2 conexiones 1GbE
-  1 conexión Infiniband FDR@56Gbps

(Nodo c7060)

1 FAT node de computación con la siguiente configuración:

-  8 procesadores Intel Haswell 8867v3, 128 cores
-  4096GB memoria
-  24 discos SAS de 1,2 TB y dos discos SAS de 300 GB

(Nodo c6714)

4 nodos de login/transfer para la conexión al sistema y la transferencia
de archivos, con la siguiente configuración cada uno:

-  2 procesadores Haswell 2680v3, 24 cores
-  128 GB memoria
-  2 discos de 1TB
-  2 conexiones 10Gbit Ethernet
-  1 conexión Infiniband FDR@56Gbps

Red de Interconexión de alto rendimiento Mellanox Infiniband FDR@56Gbps
con topología Fat-tree

Sistema de ficheros paralelo Lustre con 768TB de capacidad (750TB netos)
y 20GB/s de lectura/escritura

Consumo total de 118Kw y rendimiento pico de 328 TFlops (240 Tflops
Linpack)

|image18|

|image19|

El Fat node puede ser configurado como 4 sistemas independientes o como
sistema de memoria compartida con 128 cores, 4TB de memoria y con un
rendimiento máximo de 1.2Tflops.

Nodos integrados del SVG

Son 15 servidores cada uno con dos procesadores Intel Haswell 2650v3 (20
cores en total), 64GB de memoria y 2 discos duros de 300GB, todos los
equipos interconectados mediante dos redes Gigabit Ethernet.

Además, hay otros 8 servidores cada uno con dos procesadores Intel
Haswell 2650v3 (20 cores en total), 128 GB de memoria, 2 discos de 2TB y
tarjeta NVIDIA GRID K2 y 4 servidores cada uno con dos procesadores
Intel Haswell 2650v3, 128 GB de memoria y 2 discos de 2 TB. Los equipos
están interconectados mediante 4 enlaces Gigabit Ethernet.

Por último se han añadido 78 nodos cada uno con dos procesadores Intel
Xeon Gold 6240R (48 cores en total), 192GB de memoria, 2 disco SSD de
480GB para el sistema operativo y scratch, interconectados mediante dos
redes 10 Gigabit Ethernet.

Son 20 nodos cada uno con dos procesadores Intel Xeon Gold 6248R (48
cores en total), 192GB de memoria, 1 disco SSD de 480GB para el sistema
operativo y otro SATA de 1TB para scratch

En estos nodos, al no estar conectados mediante la red de interconexión
de alto rendimiento Mellanox Infiniband, el acceso al sistema de
ficheros paralelo Lustre tiene un rendimiento mucho menor. Debido a ello
no existe el directorio de scratch en el Lustre.

Estos nodos están accesibles a través de la partición ***shared ***\ del
sistema de colas.

Nodos AMD

Son 18 nodos cada uno con dos procesadores AMD EPYC 7452 (64 cores en
total), 256GB de memoria, 1 disco SSD de 240GB para el sistema operativo
y otro SATA de 2TB para scratch, todos los equipos interconectados
mediante dos redes Gigabit Ethernet. Al igual que sucedía con los nodos
anteriores, el acceso al sistema de ficheros paralelo Lustre tiene un
rendimiento mucho menor, por lo que no existe el directorio de scratch
en el Lustre para estos nodos.

Utilización de los nodos

Estos nodos están agrupados en la partición ***amd-shared*** y se ha
creado una nueva QOS con el mismo nombre, específica para estos nodos,
por lo tanto, para poder usar estos nodos se han de utilizar las
siguientes opciones del comando sbatch:

**-p amd-shared --qos=amd-shared**

Para conocer los límites establecidos para esta QOS se deberá ejecutar
el comando ***batchlim***.

Si se quiere utilizar una sesión interactiva en estos nodos se deberá
utilizar la opción “\ ***--amd***\ ” del comando ***compute***. Se
deberá tener en cuenta que no hay recursos dedicados para este uso
interactivo tal y como sucede con otro tipo de nodos, por lo que al
utilizarse los mismos nodos que para el resto de los trabajos, es muy
probable que haya que esperar hasta que se liberen los recursos
necesarios para esta sesión.

Aplicaciones

El conjunto de software proporcionado por el CESGA está disponible de
forma completa en estos nodos sin embargo existen **incompatibilidades**
debido al cambio en el modelo del procesador:

1. **No se recomienda usar los compiladores y librerías proporcionadas
   por Intel en estos nodos**. No se puede asegurar una correcta
   ejecución de las aplicaciones en las ramas de módulos de los
   compiladores de Intel e Intel MPI. En estos nodos se aconseja
   exclusivamente usar las aplicaciones compiladas con los compiladores
   GNU (rama de módulos de *gcc/gcccore*).
2. Parte de las aplicaciones compiladas con los compiladores GNU usan
   las **librerías Intel MKL**. En estos nodos se espera un
   **rendimiento muy bajo** de estas librerías por lo que se desaconseja
   su uso.
3. Se aconseja en los entornos de **conda **\ creados por los usuarios
   verificar si está instalado el paquete de **mkl**, si es así es
   posible un **rendimiento muy bajo** de las aplicaciones contenidas en
   estos entornos que hacen uso de las librerías MKL (por ej.
   **numpy**).

Si se necesita para el uso de estos nodos una aplicación o librería
específica que actualmente presenta estas incompatibilidades por favor
pónganse en contacto con el dpto. de aplicaciones
(aplicaciones@cesga.es) y se estudiará su instalación.

Períodos de Indisponibilidad

Estos nodos están asignados a un proyecto que puede requerir su
utilización de forma intensiva, por lo que, en ciertos períodos de
tiempo, estos nodos no estarán disponibles o estarán menos nodos
disponibles para los usuarios. Durante estos períodos de tiempo se
tratará de avisar con un mensaje al conectarse al sistema.

Nodos CL-INTEL (Cascade Lake Intel)

Son 20 nodos cada uno con dos procesadores Intel Xeon Gold 6248R (48
cores en total), 192GB de memoria, 1 disco SSD de 480GB para el sistema
operativo y otro SATA de 1TB para scratch, todos los equipos
interconectados mediante dos redes Gigabit Ethernet. Al igual que
sucedía con los nodos AMD, el acceso al sistema de ficheros paralelo
Lustre tiene un rendimiento mucho menor, por lo que no es accesible el
Lustre Scratch en estos nodos.

Utilización de los nodos

Estos nodos están agrupados en la partición ***cl-intel-shared*** y se
ha creado una nueva QOS con el mismo nombre, por lo tanto, para poder
usar estos nodos se han de utilizar las siguientes opciones del comando
sbatch:

**-p cl-intel-shared --qos=cl-intel-shared**

De inicio, estos nodos son compatibles con los nodos de la partición
***shared***, por lo que se podrían llegar a enviar trabajos solicitando
los nodos de cualquiera de las 2 particiones si los trabajos son
independientes del nodo. En ese caso, las opciones a añadir serían:

**-p shared,cl-intel-shared --qos=cl-intel-shared**

Para conocer los límites establecidos para esta QOS se deberá ejecutar
el comando ***batchlim***.

Si se quiere utilizar una sesión interactiva en estos nodos se deberá
utilizar la opción “\ ***--cl-intel***\ ” del comando ***compute***. Se
deberá tener en cuenta que no hay recursos dedicados para este uso
interactivo tal y como sucede con otro tipo de nodos, por lo que al
utilizarse los mismos nodos que para el resto de los trabajos, es muy
probable que haya que esperar hasta que se liberen los recursos
necesarios para esta sesión.

Aplicaciones

El conjunto de software proporcionado por el CESGA está disponible de
forma completa en estos nodos. Este conjunto es completamente compatible
con la arquitectura de estos nodos y se puede usar directamente. Se
recomienda el uso de los compiladores de intel así como las librerías
asociadas a la suite de intel como las MKL ya que están optimizadas para
la ejecución en esta arquitectura.

Períodos de Indisponibilidad

Estos nodos están asignados a un proyecto que puede requerir su
utilización de forma intensiva, por lo que, en ciertos períodos de
tiempo, estos nodos no estarán disponibles o estarán menos nodos
disponibles para los usuarios. Durante estos períodos de tiempo se
tratará de avisar con un mensaje al conectarse al sistema.

Nodos Finis Terrae II versus Nodos del SVG, AMD y CL-INTEL

+----------------------+------------------+-----------------+-----------------+----------------------+
| **Característica**   | **Nodos FTII**   | **Nodos SVG**   | **Nodos AMD**   | **Nodos CL-INTEL**   |
+----------------------+------------------+-----------------+-----------------+----------------------+
| Cores                | 24/128           | 4/20/24         | 64              | 48                   |
+----------------------+------------------+-----------------+-----------------+----------------------+
| Memoria              | 128G/4T          | 64G/128G        | 256G            | 192G                 |
+----------------------+------------------+-----------------+-----------------+----------------------+
| LUSTRE               | SI. Infiniband   | SI. NFS         | SI. NFS         | SI. NFS              |
+----------------------+------------------+-----------------+-----------------+----------------------+
| LUSTRE\_SCRATCH      | SI               | NO              | NO              | NO                   |
+----------------------+------------------+-----------------+-----------------+----------------------+
| MPI entre nodos      | SI               | NO              | NO              | NO                   |
+----------------------+------------------+-----------------+-----------------+----------------------+
