.. _ft2_batch_stdout_err:

Standart output and error
=========================

Slurm combina por defecto la salida estándar y de error en un único
fichero llamado ***slurm-<job\_number>.out*** o en
***slurm-<job\_number>\_<array\_index>.out*** si se trata de un job
array. Por defecto estos ficheros se guardan en el directorio de envío.
Se puede modificar este comportamiento con las siguientes opciones del
comando ***sbatch***:

***--error /path/to/dir/filename ***

***--output /path/to/dir/filename***

