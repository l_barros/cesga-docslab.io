.. _ft2_job_monitoring:

Jobs monitoring
===============

Ver trabajos en cola:

***$ squeue***

Cancelar un trabajo:

***$ scancel <job\_id>***

La salida estándar del trabajo se almacena en el fichero
***slurm-<job\_id>.out***

Consulta del histórico de trabajos:

***$ sacct***

Ver un resumen del estado de todas las particiones y trabajos:

***$ sqstat***

Es posible conectarse por ssh a los nodos en los que se está ejecutando
un trabajo para ver el estado del trabajo u otras tareas.

