.. _ft2_want_to_know_more:

Want to know more
=================

En
`*https://portalusuarios.cesga.es/info/links* <https://portalusuarios.cesga.es/info/links>`__
se pueden encontrar diversos enlaces a documentación útil como por
ejemplo, la documentación (videos y presentaciones) del ***Taller para
usuarios del Finis Terrae II***.

Para cualquier problema con el acceso al sistema:
`*sistemas@cesga.es* <mailto:sistemas@cesga.es>`__ o llamando al 981 56
98 10 y preguntando por el Departamento de Sistemas.

Para la parte de dudas de como compilar el programa o temas específicos
de MPI: `*aplicaciones@cesga.es* <mailto:aplicaciones@cesga.es>`__ o
llamando al 981 56 98 10 y preguntado por el Departamento de
Aplicaciones.
