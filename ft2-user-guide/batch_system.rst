.. _ft2_batch_system:

Batch system
=============

The queuing system is based on SLURM. For more information you can
consult the guides and tutorials available at:

Quickstart user guide:
`http://www.schedmd.com/slurmdocs/quickstart.html <http://www.schedmd.com/slurmdocs/quickstart.html>`_

Slurm Tutorials:
`*http://www.schedmd.com/slurmdocs/tutorials.html* <http://www.schedmd.com/slurmdocs/tutorials.html>`_

Useful links:
`*https://portalusuarios.cesga.es/info/links* <https://portalusuarios.cesga.es/info/links>`_

.. toctree::

   batch_basic_commands
   batch_partitions
   batch_qos
   batch_examples
   batch_memory
   batch_job_monitoring
   batch_stdout_err
   batch_email_warning

