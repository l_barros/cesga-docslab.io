.. _ft2_remote_desktops:

Annex XI: Remote desktop on a job
=================================

Actualmente en todos los nodos es posible ejecutar un escritorio remoto
bajo un trabajo en el sistema de colas. Esto habilita la posibilidad de
monitorización directa de forma gráfica e interactiva de la simulación
en curso.

Esta opción se habilita mediante la carga del módulo:

**$ module load vis/cesga**

Con el comando:

**$ module help vis/cesga**

se obtiene una breve guía de uso.

|image23|
