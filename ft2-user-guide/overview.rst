.. _ft2_overview:

Overview
==============

Finis Terrae II is a computer cluster system based on processors
Intel Haswell and interconnected via an Infiniband network with a
peak performance of 328 TFlops and sustained Linpack performance of 213 Tflops. Specific
details about its configuration in Annex I.

