.. _ft2_scratch_dirs:

Scratch dirs
==============

Además de utilizar directamente el directorio $LUSTRE de cada usuario,
en el momento de enviar los trabajos al sistema de colas se crean varios
directorios temporales que se borran automáticamente al terminar el
trabajo. Estos directorios son en el caso de los nodos del FTII:

**$LUSTRE\_SCRATCH**: directorio temporal en el almacenamiento
compartido lustre.

Nota.- Debe tener en cuenta que el espacio ocupado por los directorios
de scratch del LUSTRE también se contabilizan para el cálculo de la
cuota en el LUSTRE.

**$LOCAL\_SCRATCH** o **$TMPDIR**: directorio temporal en el disco local
de cada servidor, no recomendada su utilización.

**$TMPSHM**: directorio temporal de almacenamiento en la propia memoria
RAM del nodo (`*tmpfs* <https://en.wikipedia.org/wiki/Tmpfs>`__). Este
directorio puede usarse en el caso de necesitar realizar IO muy
intensivo sobre un fichero o conjunto de ficheros.

Nota.- Debe tener en cuenta que el espacio ocupado por los directorios
de scratch en memoria contabilizan en el límite de memoria del trabajo y
no puede superar la mitad de la memoria física del nodo.

En el caso de los nodos integrados del SVG (partición shared), los AMD
(partición amd-shared) y los CL-INTEL (partición cl-intel-shared), al no
tener acceso directo al LUSTRE, el directorio temporal en este
almacenamiento (**$LUSTRE\_SCRATCH**) no está disponible por lo que
solamente estará disponible el directorio temporal en el disco local
(**$LOCAL\_SCRATCH** o **$TMPDIR**). Por compatibilidad con los nodos
del FTII, la variable **$LUSTRE\_SCRATCH** se ha definido igual a
**$LOCAL\_SCRATCH** para estos nodos.
