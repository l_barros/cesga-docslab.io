.. _ft2_qos:

QOS
===

Los diferentes límites de recursos están asociados a QOS (Quality Of
Services) diferentes. La QOS por defecto que utilizan todos los usuarios
si no se especifica nada, es la QOS ***default***. Para utilizar otra
QOS a la hora de enviar un trabajo, deberá utilizarse la opción
***--qos=nombre\_de\_la\_qos***

Las QOS asociadas a los nodos del Finis Terrae II son las siguientes:

+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| **Name**      | **Priority**   | **MaxNodes**   | **MaxWall**   | **MaxJobsPU**   | **MaxSubmitPU**   | **MaxTRESPU**   |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| default       | 200            | 43             | 4-04:00:00    | 30              | 100               | node=129        |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| interactive   | 500            |                | 8:00:00       | 2               | 2                 | cpu=24          |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| urgent        | 500            | 4              | 2:00:00       | 5               | 50                |                 |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| long          | 200            | 43             | 8-08:00:00    | 2               | 50                |                 |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| extra-long    | 200            | 10             | 41-16:00:00   | 1               | 50                |                 |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| large         | 200            | 86             | 4-04:00:00    | 2               | 50                |                 |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| extra-large   | 200            | 256            | 4-04:00:00    | 1               | 50                |                 |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| priority      | 500            | 43             | 4-04:00:00    | 2               | 50                |                 |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+

donde:

-  **MaxNodes**: Es el número máximo de nodos que se pueden solicitar al
   enviar un trabajo.
-  **MaxWall**: Es el tiempo máximo que se puede pedir al enviar un
   trabajo.
-  **MaxJobsPU**: Es el número máximo de trabajos en ejecución por un
   usuario de forma simultánea.
-  **MaxSubmitPU**: Número máximo de trabajos en cola por un usuario de
   forma simultánea.
-  **MaxTRESPU**: Número máximo de recursos (nodos o CPUs) que puede
   usar un usuario de forma simultánea.
-  **MaxNodesPU**: Máximo número de nodos utilizados de forma simultánea
   por un usuario entre todos sus trabajos en ejecución.

En el caso de los nodos integrados del SVG, las QOS existentes son las
siguientes:

+-----------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| **Name**        | **Priority**   | **MaxNodes**   | **MaxWall**   | **MaxJobsPU**   | **MaxSubmitPU**   | **MaxTRESPU**   |
+-----------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| shared          | 200            | 5              | 4-04:00:00    | 150             | 300               | cpu=400         |
+-----------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| shared\_long    | 200            | 1              | 12-12:00:00   | 20              | 300               | cpu=100         |
+-----------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| shared\_short   | 350            | 10             | 1-00:00:00    | 200             | 300               | cpu=200         |
+-----------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| shared\_xlong   | 200            | 1              | 41-16:00:00   | 20              | 300               | cpu=20          |
+-----------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+

En este caso, al no ser ninguna de estas QOS la asignada por defecto,
deberán siempre indicar una QOS con la opción “\ ***--qos***\ ” al
enviar sus trabajos a estos nodos.

Para los nodos AMD, las QOS existentes son las siguientes:

+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| **Name**      | **Priority**   | **MaxNodes**   | **MaxWall**   | **MaxJobsPU**   | **MaxSubmitPU**   | **MaxTRESPU**   |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| amd-shared    | 200            | 1              | 2-00:00:00    | 100             | 200               | cpu=256         |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| interactive   | 500            |                | 8:00:00       | 1               | 1                 | cpu=4           |
+---------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+

Para los nodos CL-INTEL, las QOS existentes son las siguientes:

+------------------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| **Name**               | **Priority**   | **MaxNodes**   | **MaxWall**   | **MaxJobsPU**   | **MaxSubmitPU**   | **MaxTRESPU**   |
+------------------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| cl-intel-shared        | 200            | 1              | 2-00:00:00    | 100             | 200               | cpu=192         |
+------------------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+
| cl-intel-interactive   | 500            |                | 8:00:00       | 1               | 1                 | cpu=4           |
+------------------------+----------------+----------------+---------------+-----------------+-------------------+-----------------+

Todos los usuarios pueden utilizar las QOS ***amd-interactive,
amd-shared, cl-intel-interactive, cl-intel-shared, default***\ **,
**\ ***interactive***\ **,** ***urgent, shared, shared\_long*** y
***shared\_short***. Las otras QOS deben solicitarse a través de
solicitud de recursos especiales:

Estos límites pueden cambiar con el paso del tiempo, por lo que para
conocer los límites existentes asociados a su cuenta puede ejecutar el
comando ***batchlim***.



