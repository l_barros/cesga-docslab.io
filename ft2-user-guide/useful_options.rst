.. _ft2_useful_options:

Useful Options
==============

***squeue --start***: Muestra la hora estimada de comienzo de los
trabajos en espera

***sqstat:*** Muestra información detallada de las colas y la
utilización global del equipo.

