.. _ft2_annex_5_multiple_tasks_on_job:

Annex V: Multiple tasks on a job
================================

Trabajos que consisten en la ejecución de varias tareas preferiblemente
deberían ser combinados de alguna forma para optimizar el envío de
trabajos y reducir así en número de trabajos enviados a un número más
manejable.

A continuación mostramos dos formas de llevar a cabo este tipo de
trabajos.

Ejecución de múltiples programas dentro de un trabajo con la opción
--*multi-prog* de *srun*

Información

La opción ***--multi-prog*** del comando ***srun*** nos permite ejecutar
múltiples programas dentro de único trabajo.

Se puede obtener información sobre esta opción ejecutando el comando
“\ ***man srun***\ ” y buscando “multi-prog” en el manual mostrado.

También se puede encontrar más información en páginas como estas:

-  `*http://www.tchpc.tcd.ie/node/167* <http://www.tchpc.tcd.ie/node/167>`__
-  `*https://computing.llnl.gov/tutorials/linux\_clusters/multi-prog.html* <https://computing.llnl.gov/tutorials/linux_clusters/multi-prog.html>`__

Ejemplo simple:

/opt/cesga/job-scripts-examples/Simple\_Multiprog\_Job.sh

/opt/cesga/job-scripts-examples/simple\_multiprog.config

Envío del trabajo

Para enviar un trabajo con el *sbatch* necesitaremos como primer paso
crear el script que vamos a utilizar. En el siguiente ejemplo vamos a
solicitar la utilización de 8 cores en 1 nodo (es decir un total de 8
tareas) pero sería fácilmente adaptable al número de nodos y cores
deseados utilizando las opciones necesarias del comando *sbatch*.

El script a utilizar lo llamaremos test.sh y será algo como esto:

| #!/bin/sh
| #SBATCH -n 8 # 8 cores
| #SBATCH -t 10:00:00 # 10 hours
| #SBATCH -p thin-shared # partition name
| srun --multi-prog test.config

El fichero ***test.config*** contiene los parámetros requeridos por la
opción multi-prog.

\ **Fichero de configuración**

El fichero de configuración contiene 3 campos, separados por espacios.
Los campos son:

-  El número de tarea
-  El programa/script a ejecutar
-  Argumentos

Parámetros disponibles:

-  %t - The task number of the responsible task
-  %o - The task offset (task's relative position in the task range).

Para este ejemplo utilizaremos el siguiente fichero de configuración:

###################################################################

# srun multiple program configuration file

#

# srun -n8 -l --multi-prog test.config

###################################################################

0 /home/cesga/prey/multiprog/script.sh %t

1 /home/cesga/prey/multiprog/script.sh %t

2 /home/cesga/prey/multiprog/script.sh %t

3 /home/cesga/prey/multiprog/script.sh %t

4 /home/cesga/prey/multiprog/script.sh %t

5 /home/cesga/prey/multiprog/script.sh %t

6 /home/cesga/prey/multiprog/script.sh %t

7 /home/cesga/prey/multiprog/script.sh %t

***Nota**.- Nótese que en el caso de utilizar un ejecutable/script
propio como en este caso, se deberá indicar la ruta completa al
fichero.*

***Nota**.- El número de tareas indicado en este fichero de
configuración ha de ser exactamente el mismo que el número de tareas
solicitadas mediante sbatch.*

En este ejemplo, se indica que queremos ejecutar un script (*script.sh*)
que requiere el uso de un core y al cual se le pasa como argumento el
número de tarea, el cual podemos utilizar de modo similar a como se hace
con los job-arrays para identificar algún parámetro necesario en la
ejecución.

Es posible indicar que el script utilice más de un core para su
ejecución utilizando la opción “\ *-c NCORES*\ ” y además es posible
indicar diferentes programas/scripts dentro de este fichero de
configuración en donde para cada entrada se utilice un script diferente.
En la información indicada al inicio de esta sección tiene ejemplos de
ello.

Para el ejemplo mostrado anteriormente, dado que se trata de ejecutar el
mismo script para todas las tareas podríamos simplificarlo poniendo una
única línea como la siguiente:

0-7 /home/cesga/prey/multiprog/script.sh %t

Si utilizamos por ejemplo el siguiente script:

#!/bin/sh

echo "TAREA=$1"

obtendremos la siguiente salida en la cual se puede ver el identificador
de cada tarea:

TAREA=7

TAREA=0

TAREA=1

TAREA=4

TAREA=5

TAREA=3

TAREA=2

TAREA=6

***Nota**.- El orden en el que salen reflejadas las tareas puede ser
diferente entre ejecuciones del mismo trabajo.*

Trabajos multistep y opción –r,--relative

Ejecución de diferentes programas a la vez en distintos recursos dentro
de un trabajo. Varios srun pueden ejecutarse al mismo tiempo dentro de
un trabajo concreto siempre que no superen los recursos reservados para
ese trabajo:

#!/bin/bash

#SBATCH -n 8 # 8 tasks

#SBATCH --ntasks-per-node=4 # 2 Nodes

#SBATCH -t 10:00 # 10 min

#SBATCH -p thinnodes,cola-corta # partition

#STEP 0

srun -n4 sleep 60 &

#STEP 1

srun -n4 sleep 60 &

wait

En este caso las 4 tareas del step 0 se ejecutarán con una distribución
en bloque, 3 tareas en el primer nodo y una en el segundo y las 4 tareas
del step 1 esperarán a la finalización de las tareas del step 0 y se
ejecutarán con la misma distribución en los 2 nodos asignados al
trabajo. Sin embargo con la siguiente modificación:

#STEP 0

srun -N1 -n4 sleep 60 &

#STEP 1

srun -N1 -r1 -n4 sleep 60 &

las tareas del step 0 se ejecutarán en el primer nodo y las del segundo
step en el segundo nodo. Nótese la necesidad de especificar -N1 para
definir el nº de nodos donde se ejecutarán las tareas del step 0 dentro
de los recursos del trabajo y -N1 -r1 en el caso del step 1 para definir
el nº de nodos y la posición relativa de estos nodos dentro de los
recursos asignados al trabajo.

**-r, --relative=<n>**

Run a job step relative to node n of the current allocation. This option
may be used to spread several job steps out among the nodes of the
current job. If -r is used, the current job step will begin at node n of
the allocated nodelist, where the first node is considered node 0. The
-r option is not permitted with -w or -x option and will result in a
fatal error when not running within a prior allocation (i.e. when
SLURM\_JOB\_ID is not set). The default for n is 0. If the value of
--nodes exceeds the number of nodes identified with the --relative
option, a warning message will be printed and the --relative option will
take precedence.

Ejemplo combinado multiprog/multistep:
**/opt/cesga/job-scripts-examples/Multiprog\_Job.sh**

Ejecución de múltiples programas dentro de un trabajo utilizando *GNU
Parallel* y *srun*

Información

Cuando el número de tareas a realizar es muy grande y dichas tareas no
duran todas tiempos similares, la opción anterior no parece la más
adecuada sino que sería más conveniente la utilización conjunta de *GNU
Parallel* y *srun*. Con este método de envío, se enviará un único
trabajo con un número elegido de tareas (usando la opción --*ntasks=X*)
y el comando ***parallel*** se utilizará para ejecutar ese número de
tareas simultáneamente hasta que todas las tareas se hayan completado.
Cada vez que acabe una tarea, otra será puesta en marcha sin tener que
esperar a que finalicen todas.

Puede encontrar más información en la siguiente página:

`*https://rcc.uchicago.edu/docs/running-jobs/srun-parallel/index.html#parallel-batch* <https://rcc.uchicago.edu/docs/running-jobs/srun-parallel/index.html#parallel-batch>`__

`****** <https://rcc.uchicago.edu/docs/running-jobs/srun-parallel/index.html#parallel-batch>`__

Independientemente de la partición y recursos solicitados en el trabajo
las opciones srun y GNUparallel recomendadas son:

#shared partitions

MEMPERCORE=$(eval $(scontrol show partition $SLURM\_JOB\_PARTITION
-o);echo $DefMemPerCPU)

if [ -z "$MEMPERCORE" ]

 then

 #exclusive partitions

 MEMPERCORE=$(( $(sinfo -e -p $SLURM\_JOB\_PARTITION -o "%m/%c" -h) ))

fi

export OMP\_NUM\_THREADS=${SLURM\_CPUS\_PER\_TASK=1}

#SRUN="srun -N1 -n1 --mem=$(( $MEMPERCORE\*$OMP\_NUM\_THREADS )) -c
$OMP\_NUM\_THREADS --cpu\_bind=none"

SRUN="srun --exclusive -N1 -n1 --mem=$(( $MEMPERCORE\*$OMP\_NUM\_THREADS
)) -c $OMP\_NUM\_THREADS"

# **--delay .2** prevents overloading the controlling node

# **-j** is the number of tasks parallel runs so we set it to
$SLURM\_NTASKS

# **--joblog** makes parallel create a log of tasks that it has already
run

# **--resume** makes parallel use the joblog to resume from where it has
left off

# the combination of --joblog and --resume allow jobs to be resubmitted
if

# necessary and continue from where they left off

parallel="parallel --delay .2 -j $SLURM\_NTASKS --joblog
logs/runtask.log --resume"

# this runs the parallel command we want

# in this case, we are running a script named runGP.sh

# parallel uses ::: to separate options. Here **{0..99}** is a shell
expansion

# so parallel will run the command passing the numbers 0 through 99

# via argument {1}

$parallel "$SRUN ./task.sh {1} > logs/parallel\_{1}.log" ::: {0..99}

Estas opciones usan parallel para ejecutar múltiples srun simultáneos.
En estas opciones se contempla la ejecución de tareas secuenciales (uso
de un único core) o también la ejecución de tareas multicore ó
multiproceso ajustando adecuadamente la petición de recursos del comando
sbatch:

#SBATCH -n 12 #(12 tareas de 1 único core por tarea)

#SBATCH -n 12 -c4 #(12 tareas multicore con 4 cores por tarea)

La opción -*-exclusive* es necesaria en la ejecución de distintos job
steps dentro de un mismo nodo ya que asegura el uso único de los
recursos por cada tarea. Otra posibilidad es el uso de la opción
*--cpu\_bind=none* de manera que el sistema operativo sea el responsable

Ejemplo:

**/opt/cesga/job-scripts-examples/GNUParallel.sh**

**/opt/cesga/job-scripts-examples/task.sh**
