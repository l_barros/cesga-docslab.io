.. _ft2_job_arrays:

Annex III: Job arrays
=====================

Un job array en Slurm implica que el mismo script va a ser ejecutado “n”
veces, la única diferencia entre cada ejecución es una simple variable
de entorno, “\ ***$SLURM\_ARRAY\_TASK\_ID”***.

Esta opción simplifica el envío de “n” trabajos similares y le
proporciona al usuario una gestión única simulando un único trabajo pero
internamente slurm los trata como “n” trabajos independientes y así
contribuyen en todos los límites del usuario (partición y QoS).

Opción:

**-a, --array=<indexes>**

Submit a job array, multiple jobs to be executed with identical
parameters. The indexes specification identifies what array index values
should be used. Multiple values may be specified using a comma separated
list and/or a range of values with a "-" separator. For example,
"**--array=0-15**" or "**--array=0,6,16-32**". A **step function** can
also be specified with a suffix containing a colon and number. For
example, "**--array=0-15:4**" is equivalent to "**--array=0,4,8,12**". A
**maximum number of simultaneously running tasks** from the job array
may be specified using a "%" separator. For example "**--array=0-15%4**"
will limit the number of simultaneously running tasks from this job
array to 4. The minimum index value is 0. the maximum value is one less
than the configuration parameter MaxArraySize.

**INPUT ENVIRONMENT VARIABLES**

SBATCH\_ARRAY\_INX: Same as -a, --array

**OUTPUT ENVIRONMENT VARIABLES**

SLURM\_ARRAY\_TASK\_ID : Job array ID (index) number.

SLURM\_ARRAY\_JOB\_ID : Job array's master job ID number.

SLURM\_ARRAY\_TASK\_COUNT: Total number of tasks in a job array.

SLURM\_ARRAY\_TASK\_MAX: Job array's maximum ID (index) number.

SLURM\_ARRAY\_TASK\_MIN: Job array's minimum ID (index) number.

SLURM\_ARRAY\_TASK\_STEP: Job array's index step size.

**Ejemplo: /opt/cesga/job-scripts-examples/Job\_Array.sh**
