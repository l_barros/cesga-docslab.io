.. _ft2_drop_caches:

Annex VIII: Drop caches
=======================

Al final de cada trabajo de la partición thinnodes se ejecuta un drop
caches que limpia la cache de los nodos para evitar caídas del
rendimiento entre trabajos.
