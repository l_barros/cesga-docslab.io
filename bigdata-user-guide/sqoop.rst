.. _sqoop:

Sqoop
=====

Sqoop allows to easily import data from relational databases into HDFS.

We have already deployed the Sqoop connectors for the following databases:

- MySQL / MariaDB
- PostgreSQL
- Microsoft SQL Server
- Oracle 18c

This way, out of the box you can use the Sqoop tool to import data from any of these databases::

    sqoop import \
        --username ${USER} --password ${PASSWORD} \
        --connect jdbc:postgresql://${SERVER}/${DB} \
        --table mytable \
        --target-dir /user/username/mytable \
        --num-mappers 1

.. note:: We recommend that you use only one mapper process to avoid overloading your database.

If you need to import data from a different database don't hesitate to contact us.

For further information on how to use Sqoop you can check the `Sqoop Tutorial`_ that we have prepared to get you started and the `Sqoop Guide`_ in the CDH documentation.

.. _Sqoop Tutorial: https://bigdata.cesga.es/tutorials/sqoop.html
.. _Sqoop Guide: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/sqoop.html
