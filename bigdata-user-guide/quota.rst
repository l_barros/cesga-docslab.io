.. _quota:

Filesystem Quotas
=================
The filesystems have usage quotas that limit both the maximum allowed number of files and the total space used.

To see your current filesystem quotas and how close you are to reach the limits you can use the **myquota** command::

	[sistemas@cdh61-login8 ~]$ myquota 

	 - HOME and Store filesystems:
	   --------------------------
	Filesystem                   space  quota  limit  files  quota  limit
	10.117.49.101:/Home_BD/home  600K   800G   1024G  47     4295m  4295m

	 - HDFS filesystem:
	   -----------------
	 FILES QUOTA        REMAINING    SPACE QUOTA        REMAINING        DIRS        FILES               SIZE             
		  39.1 K          39.1 K            18 T            18 T            1            0                  0 /user/sistemas


To avoid unexpected failures in your jobs we recommend that you verify that you have enough space for your jobs before submitting them.

If you are close to reach your quota you need to increase the limits you can do an `Additional Storage Request`_.

.. _Additional Storage Request: https://www.altausuarios.cesga.es/solic/almalta.php
