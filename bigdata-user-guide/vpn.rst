.. _VPN:

ANNEX I: VPN
============

The VPN software allows you not only to connect to CESGA in a secure way but also to access internal resources that you can not reach otherwise.

Installing the Forticlient VPN software
---------------------------------------
Depending on your Operating System the steps will be different:

- For Windows/Mac download the FortiClient VPN software (the VPN-only version) from the `Official Forticlient download page`_ (scroll down until you find the section "Forticlient VPN", the VPN-only version of FortiClient) and launch the installation wizard.
- For recent Linux versions like Ubuntu 18.04 and Centos 7 or newer we recommend to use the `Open Source Linux Client OpenfortiVPN`_.
  If you prefer to install the official Fortinet client you you can follow the `Forticlient official instructions`_.
- For very old Linux versions check the `old linux versions`_ section.

In all cases, enter the following configuration options:

  - Gateway: gateway.cesga.es
  - Port: 443
  - Username: your **email address** registered at CESGA (do not use your username)
  - Password: your password in the supercomputers

Remember to start the VPN before connecting to **hadoop3.cesga.es**.

.. _Official Forticlient download page: https://www.forticlient.com/downloads
.. _Forticlient CESGA Repository: https://portalusuarios.cesga.es/layout/download/vpn-fortissl.rar
.. _Forticlient official instructions: https://www.forticlient.com/repoinfo


.. _`Open Source Linux Client OpenfortiVPN`:

Open Source Linux Client OpenfortiVPN
-------------------------------------
For Linux there is also an alternative open-source client called OpenFortiVPN_ that you can use instead of the official fortivpn client.
Some Linux distibutions like Ubuntu, Debian, OpenSuse or Arch Linux provide OpenFortiVPN packages.

It has also a QT GUI that you can use OpenFortiGUI_, and it can be natively integrated with NetworkManager in GNOME using NetworkManager-fortisslvpn_.

If you are using Ubuntu 18.04 or newer you can install it using::

    sudo apt install openfortivpn

If you want to use the integration with the NetworkManager in GNOME you can install also::

    sudo apt install network-manager-fortisslvpn-gnome

The package needed are in the *universe* repo, which is usually enabled by default.

If you are using Centos 7, it is available in the *EPEL* repo. Once this repo is enabled you can install it using::

    yum install openfortivpn

Once openfortivpn is installed you can start the VPN executing::

   sudo openfortivpn gateway.cesga.es:443 -u curso001@cesga.es

where *curso001@cesga.es* should be replaced by the email address that you used to register at CESGA.

Check the project github page for details OpenFortiVPN_.

.. _OpenFortiVPN: https://github.com/adrienverge/openfortivpn
.. _OpenFortiGUI: https://hadler.me/linux/openfortigui/
.. _NetworkManager-fortisslvpn: https://github.com/GNOME/NetworkManager-fortisslvpn

.. _old Linux versions:

VPN Installation in old Linux versions
--------------------------------------
If your Linux distribution no longer appears in the `Official Forticlient download page`_ you can use at your own risk the `Forticlient CESGA Repository`_ that maintains old versions of the software that can contain security vulnerabilities.

.. warning::
  Our recommendation is that you update your linux distribution as soon as possible.

Follow the next steps to do the installation::

  unrar e vpn-fortissl.rar
  tar xvzf forticlientsslvpn_linux_4.4.2323.tar.gz
  cd forticlientsslvpn
  ./fortisslvpn.sh

Accept the license agreement presented.

After the installation you can start the VPN from the command line::

  ../forticlientsslvpn_cli --server gateway.cesga.es:443 --vpnuser usuario@dominio.com


