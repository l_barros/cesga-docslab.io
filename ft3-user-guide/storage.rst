.. _ft3_storage:

Storage
=======
The storage system is divided in three principal partitions, each one with different memory space and file limits. To avoid unexpected failures we recommend that you verify that you have enough space for your jobs before submitting them. Special care must be observed in the HOME quota. Inability to write to the home will cause many services and applications to crash.

If you are close to reach your quota and you need to increase the limits, you can ask for an `Additional Storage Request`_.

.. _Additional Storage Request: https://altausuarios.cesga.es/solic/almac 
.. note::

  $HOME and $STORE directories are **shared** between FinisTerrae II and FinisTerrae III.

+--------------------+---------------------------------+---------------+--------+
| Directory          | Use                             | User limits   | Backup | 
+--------------------+---------------------------------+---------------+--------+
|                    | Store code files                | 10GB          | Yes    | 
|      $HOME         |                                 |               |        |                     
|                    | Low speed access                | 100.000 files |        |                      
+--------------------+---------------------------------+---------------+--------+
|                    | Store simulations final results | 500GB         | No     |               
|      $STORE        |                                 |               |        |                      
|                    | Low speed access                | 300.000 files |        |                      
+--------------------+---------------------------------+---------------+--------+
|                    | Simulation runs                 | 3TB           | No     |              
|      $LUSTRE       |                                 |               |        |                      
|                    | High speed access               | 200.000 files |        |                      
+--------------------+---------------------------------+---------------+--------+


Filesystem Quotas
-----------------
The filesystems have usage quotas that limit both the maximum allowed number of files and the total space used.
To see your current filesystem quotas and how close you are to reach the limits you can use the ``myquota`` command.

.. code-block:: console

  [swtest@login210-19 ~]$ myquota 

    - HOME and Store filesystems:
      --------------------------
  Filesystem               space  quota   limit   files  quota  limit
  10.117.49.201:/Home_FT2  3200M  10005M  10240M  42506  100k   101k
  10.117.49.101:/Home_BD   8K     800G    1024G   7      4295m  4295m
  
   - Special filesystems:
     -------------------
  Filesystem                  Size  Used Avail Use% Mounted on
  10.117.49.101:/Store_CESGA   13T  9.2T  3.9T  71% /mnt/netapp1/Store_CESGA
  total                        13T  9.2T  3.9T  71% -
  
   - LUSTRE filesystem:
     -----------------
  Filesystem    used   quota   limit   grace   files   quota   limit   grace
  /mnt/lustre/scratch
                     4k      3T    3.5T       -       1  200000  240000       -
