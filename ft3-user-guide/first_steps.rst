.. _ft3_first_steps:

First steps
===================

Creating an account
--------------------

The first step is to `register as user. <https://altausuarios.cesga.es/user/ualta>`_ We highly recommed you to follow the steps listed on the website and check out the user registration's diagram in order to avoid any issues with your application. 

If you are a member of CSIC and you want to create a group you have to request a `group registration <https://altausuarios.cesga.es/user/galta>`_ and follow the steps. 

Connection from an unauthorized center
----------------------------------------------
For security reasons, access to our servers is restricted to authorized centers (Galician universities,  CSIC centers, centers with special agreements ...), so access to them is only possible from these registered centers and users. Out of these hubs, the connection must be made using VPN and you can configure it following the steps in `how to connect. <https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html#configure-vpn>`_

This `authorization request <https://altausuarios.cesga.es/solic/conex>`_ is intended for new center asociations or for users which need mandatory the registration of their public IP to granteed access due to technical problems.

