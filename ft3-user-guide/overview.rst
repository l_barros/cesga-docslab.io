.. _ft3_overview:

Overview
========

Finisterrae III with a total computing power of 4,36 PetaFLOPS is made up of 431 nodes interconnected via an Infiniband HDR network. There are 714 latest generation Intel Xeon Ice Lake 8352Y processors with 32 cores at 2.2Ghz (22.848 cores) and 157 GPUs (141 Nvidia A100 and 16 Nvidia T4).

.. figure:: _static/screenshots/FT3_schema.jpg 
   :align: center

In addition to these nodes, a series of nodes recovered from previous clusters are available, such as the following:

- 74 nodes with 2 Intel Xeon Gold 6240R processors with 24 cores at 2.40Ghz (that is, 48 cores by node) and 180GB of RAM (called **clk nodes**)

.. warning:: MPI jobs using multiple nodes are not allowed in this partition
