.. _ft3_data_transfer:

Data transfer
=============
For files transfer to or from the FinisTerrae III an *scp* or *sftp* client must be used. Other possibility it is to use the ``rsync`` command explained below. LUSTRE directories of FinisTerrae II and FinisTerrae III are **different** systems so you have to copy the desired data from FinisTerrae II to FinisTerrae III.

.. warning:: The shutdown of FinisTerrae II is planned for september 2022. After this date its operation cannot be guaranteed, especially the data stored in its LUSTRE. Therefore, all users are advised to migrate their files to FinisTerrae III's LUSTRE, as indicated below.

SCP and SFTP clients
--------------------
As explained in `how to connect, <https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html>`_ **MobaXterm** is a tool that can provide both SSH access and file transfer. It's interface makes easy data transfer avoiding using commands especially for new users. The basic mechanism is to go to the FinisTerrae III directory to which you want to move the files in the left panel of the program interface. The next step will simply be to drag the files from your folder on the local computer to that panel. 

**WinSCP** is a similar tool but this just provides data transfering. At first, you have to configured the conection to FinisTerrae III as follow: 

.. code-block:: console

        Protocol: SFTP
        Name or server IP: ft3.cesga.es
        Port: 22
        Username: username (just the name, not the full email)
        Password: your_password

Once connected, you will have your local computer on the left and FinisTerrae III directories on the right. The data transfer is also dragging fileS for one directory to other. You can also navegate between directories on both sides. 
There are more SCP and SFTP clients which can be used, these two are just examples.

Migrate LUSTRE data from FinisTerrae II to FinisTerrae III
----------------------------------------------------------
To transfer data between both machines you can use commands like ``scp`` or ``rsync``. You can use commands like the following examples:

.. code-block:: console

        $ scp -rp $LUSTRE/* ft3.cesga.es:PATH_LUSTRE_FT3/

        $ rsync -avHAP $LUSTRE/ ft3.cesga.es:PATH_LUSTRE_FT3/


To obtain the value of *PATH_LUSTRE_FT3* you can execute the command ``echo $LUSTRE**`` in FinisTerrae III.

In case of interruption, running the ``rsync`` command will resume the transfer from the point where it was cut off. The recommendation is to run it several times to ensure that no files remain unsynchronized.

It could be recommended to use the command **screen** to execute these commands to transfer these data, because it allows you to resume the sessions. Screen is a terminal multiplexer. In other words, it means that you can start a screen session and then open any number of windows (virtual terminals) inside that session. Processes running in Screen will continue to run when their window is not visible even if you get disconnected. If you use this command, you have to remenber the login node where you have launched it, to be able to resume your screen session.
