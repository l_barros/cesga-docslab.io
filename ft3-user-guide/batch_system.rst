.. _ft3_batch_system:

Batch system
=============

The queuing system is based on SLURM, an open source, fault-tolerant and highly scalable cluster management and job scheduling system for Linux clusters. 

Slurm has three key functions:
   1. It allocates exclusive and/or non-exclusive access to resources (compute nodes) to users for some duration of time.
   2. It provides a framework for starting, executing and monitoring jobs on the set of allocated nodes. 
   3. It arbitrates contention for resources by managing a queue of pending work.
For more information about SLURM you can consult the guides and tutorials available on their website: `Quickstart user guide <http://www.schedmd.com/slurmdocs/quickstart.html>`_ and  `SLURM tutorials. <http://www.schedmd.com/slurmdocs/tutorials.html>`_ There's also some useful links at `portalusuarios.cesga.es <https://portalusuarios.cesga.es/info/links>`_ 

**Index of contents:**

.. toctree::

   batch_memory
   batch_basic_commands
   batch_partitions
   batch_qos
   batch_job_array
   batch_multiple_tasks
   batch_examples
   batch_job_monitoring
   batch_jobs_states
   batch_stdout_err
   batch_email_warning
   batch_useful_options

