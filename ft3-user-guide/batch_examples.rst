.. _ft3_batch_examples:

Script examples
========

It is mandatory to specify the maximum execution time and the memory needed as explained in `memory and time <https://cesga-docs.gitlab.io/ft3-user-guide/batch_memory.html>`_ section. 

For the further examples lets set::

    export SBATCH_MEM_PER_CPU=1G

Using srun
----------

.. code-block::
    
   $ srun -n2 --time=00:00:10 hostname

Request two tasks (-n2) and execute the command *hostname* with
a maximum execution time of 10 seconds. Its use is not recommended
since it blocks the prompt until the complete execution of the job.

Using sbatch
------------

Generate an script *job.sh* containing::

    #!/bin/sh
    #SBATCH -n 2 #(2 tasks in total)
    #SBATCH -t 00:00:30 #(10 sec max wall time)
    #SBATCH --mem-per-cpu=1G
    srun hostname

and submmit the job with the command::

    $ sbatch ./job.sh

sbatch options can be changed at submission time. For instance, to s
ubmit the above script asking for 4 tasks::

    $ sbatch -n 4 ./job.sh

OpenMP job submission
---------------------

- Compilation::

    $ module load intel
    $ ifort -qopenmp -o omphello_f /opt/cesga/job-scripts-examples/omphello.f
    $ icc -qopenmp -o omphello_c /opt/cesga/job-scripts-examples/omphello.c

- Submission script (run.sh)::

    #!/bin/bash
    #SBATCH -n 1 # (1 task)
    #SBATCH -c 8 # (8 cores per task)
    #SBATCH -t 00:10:00 # (10 min max wall time )
    #SBATCH --mem-per-cpu=1G
    ./omphello_f

- Submission command::

    $ sbatch run.sh

MPI job submission
------------------

- Compilation::

    $ module load intel impi
    $ mpiifort -o pi3 /opt/cesga/job-scripts-examples/pi3f90.f90

- Submission script (run.sh)::

    #!/bin/bash
    #SBATCH -n 16
    #SBATCH --ntasks-per-node=8
    #SBATCH -c 8
    #SBATCH -t 00:10:00
    #SBATCH --mem-per-cpu=1G
    module load intel impi
    srun ./pi3

2 nodes are requested, using 16 processes (*-n 16*), 8 processes per 
node (*--ntasks-per-node=8*) and 8 cores per process (*-c 8*, in case 
the program can use this type of hybrid parallelization), in total 128 
cores (2 nodes).

- Submission script demanding exclusive nodes (run.sh)::

    #!/bin/bash
    #SBATCH -n 16
    #SBATCH --ntasks-per-node=8
    #SBATCH -c 4
    #SBATCH --exclusive
    #SBATCH -t 00:10:00
    #SBATCH --mem-per-cpu=1G
    module load intel impi
    srun ./pi3

2 exclusive nodes are requested, using 16 processes (*-n 16*), 8 processes per 
node (*--ntasks-per-node=8*) and 4 cores per process, in total 64 
cores (32 cores per node).

- Submission command::

    $ sbatch run.sh


