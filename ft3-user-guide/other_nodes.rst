.. _ft3_other_nodes:

Other nodes
===========

This section describe how to use other nodes added to Finisterrae III.

clk nodes
---------

74 nodes with 2 Intel Xeon Gold 6240R processors with 24 cores at 2.40Ghz (that is, 48 cores by node) and 180GB of RAM.

To use these nodes you have to add the option ``-C clk`` when you submit a job with the ``sbatch`` command. Example::

    $ sbatch -C clk -t 24:00:00 --mem=4GB script.sh

These nodes, since they are not connected via the high-performance Mellanox Infiniband interconnect network, access to LUSTRE directories has lower performance. So if your jobs are I/O intensive in LUSTRE, they may be affected on these nodes.  

.. warning:: MPI jobs using multiple nodes are not allowed in this partition
