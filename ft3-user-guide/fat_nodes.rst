.. _ft3_fat_node:

Fat nodes
=========

  - **smp**: 16 nodes with 64 cores and 2011GB of RAM available

  - **optane**: 1 node with 64 cores and 7975GB of Optane memory available. It should be taken into account that the performance of this memomy is lower than the RAM memory, so execution time in this node can be greater. Access to this node is restricted, so if you wish to use it, you must send an email to `sistemas@cesga.es <mailto:sistemas@cesga.es>`_

These nodes will be assigned automatically by the queue system if you required more than 100GB of RAM by core or if you request more RAM than a basic *ilk* node has (247GB).
