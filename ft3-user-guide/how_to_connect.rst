.. _ft3_how_to_connect:

How to connect
==============
Access to our servers is done using an SSH (Secure Shell) client that allows encrypted information transfers. SSH allows the conection between computers over the network, executing commands on the remote machine and moving files from one machine to another. Provides strong authentication and secure communications over non-secure channels. All communications are automatically and transparently encrypted, including passwords. 

This prevents password capture, one of the most common means by which computer system security is compromised. Most versions of SSH provide a remote copy (SCP) operation, and many also provide a secure ftp (sftp) client. Additionally, SSH allows secure X-Windows connections.

Configure the VPN
--------------
To be able to access the Finisterrae III after you have to configure the VPN with Forticlient. This step will be mandatory in all those cases in which the user connects from an unauthorized center (users who connect from home, from their work center or from a university center). The installation and configuration of this tool is discussed below for different operating systems.

.. note::
    If the configuration of the VPN is not working due to technical problems, you can connect to our servers via `User Portal <https://portalusuarios.cesga.es/>`_ with your username and password. In Tools head, you have an `SSH Terminal <https://portalusuarios.cesga.es/info/web_ssh>`_  or a `Remote Desktop <https://portalusuarios.cesga.es/tools/remote_vis>`_ but you should keep in mind that these tools are meant to be a supporting option to be used in special cases. Remote desktops will be destroyed after 24 hours unless you log out of the desktop before reaching that limit or restart within that time.

Installation and configuration of FortiClient on Windows and MacOS
------------------------------------------------------------------

.. warning:: If the client freezes at 40%, it is due to a bug in FortiClient version 7.0 or later. To fix this error, we recommend downloading and installing version **6.2** for `Windows <https://cumulo.cesga.es/index.php/s/AFwnC7SSDzesFqg>`_ and `MacOS <https://cumulo.cesga.es/index.php/s/XDXm7nMW4qoS5rb>`_ .


Install FortiClient as VPN client provider which can be downloaded from their website. We will configure the VPN connection as follows (some differences between the Windows client and MacOS client could appear but the parameters of configuration are the same):

.. figure:: _static/screenshots/vpn1.png 
   :align: center

For its configuration it is necessary to establish the VPN name as **gateway.cesga.es**. You have to **check the Customize port box** (with the number **443**) since it is not checked by default even if you write the number. 

.. note::
   
   It's important to underline that the **username** is the **complete email** with which the user **requested the registration** to access our servers. The domain may change between institutions, universities and other associated centers. If you have doubts about which is your registration email, you can check it in the `User Portal <https://portalusuarios.cesga.es/user/perfil>`_ 
   
   For the users with a "curso" account, the login will be cursoNNN@cesga.es being NNN the number given to that account. 
   
Installation and configuration of FortiClient on Linux
------------------------------------------------------

1. Download Forticlient `here <https://portalusuarios.cesga.es/layout/download/vpn-fortissl.rar>`_ an follow these steps as root:

.. code-block:: console

    unrar e vpn-fortissl.rar
    tar xvzf forticlientsslvpn_linux_4.4.2323.tar.gz
    cd forticlientsslvpn
    ./fortisslvpn.sh
    Accept the license agreement presented
    ../forticlientsslvpn_cli --server gateway.cesga.es:443 --vpnuser usuario@dominio.com

Alternatively if you have a Linux OS (Debian, Ubuntu or Mint), you can install **network-manager-fortisslvpn**. This package belongs to the 'universe' repository which is normally enabled by default. Another option is to download **openfortivpn** from its website since is not available on package. 

On Linux systems, to create the VPN from the GNOME network manager, it is also necessary to install the network-manager-fortisslvpn-gnome package. Without that package the "VPN" tab does not appear within the fortisslvpn configuration.

2. For any of the cases above, configure the connection as root as follows:

.. code-block:: console

    Gateway Remoto: gateway.cesga.es
    Port: 443
    Username: email@dominio.com (it is the one used in the user registration)
    Password: your_password

No certificated are need to stablish the connection. 

.. Note:: Finis Terrae III: ft3.cesga.es (Fingerprint SSH: SHA256:LeEPzn5dC89HQ/54mnSKnAqam/cLNiiNqZS/MosZ7VY)

Remote connection
-----------------
If you are using Linux or MacOS as your operating system, you can access to Finisterrae III directly from the command terminal but if you are a Windows user, you will need to install a UNIX like environment.

**MobaXterm** is a tool that will provide a complete environment for connecting to the Finisterrae III and transferring files local//Finisterrae III. You can download and install MobaXterm from its official page. Once installed, the first step would be to create a new user session, configuring it as follows:

.. code-block:: console

    Remote Host: ft3.cesga.es
    Specify username: username (Just the username without @)
    Port: 22

Next, a new tab will open in the terminal, in which you will be asked for the password of your account. For security, when the password is typed the cursor does not move nor shows any type but the password is being written.

Once the password is verified, it will show you the Finisterrae III home screen and you will be in one of the login nodes, from which you can edit and copy/move files and submit jobs to the queue system. These nodes are not intended for job execution, and each session is CPU time-limited to 8 hours and 8GB of memory of virtual memory. For larger needs, a node should be used interactive.


The connection to the FinisTerrae III login nodes must be made via an ssh client, using the following address:

.. code-block:: console

    ssh username@ft3.cesga.es

By default, the session is started at the $HOME directory but you can change between directories using:

.. code-block:: console

    cd $STORE
    cd $LUSTRE
    cd (To go back to $HOME)

