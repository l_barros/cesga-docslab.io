.. CESGA FT 3 User's Guide documentation master file

FinisTerrae III User Guide
==========================

.. |ft3| image:: _static/screenshots/ft3.png
    :width: 320px
    
.. |ft3data| image:: _static/screenshots/ft3_b.png
    :width: 260px

.. |federft3| image:: _static/screenshots/Cartel-FEDER-FTIII-800x571.png
    :width: 330px
    
.. |federft3data| image:: _static/screenshots/Cartel-FEDER-STORAGE-800x571.png
    :width: 330px

+---------------------------+
| |ft3|       |ft3data|     |
+---------------------------+   
| |federft3| |federft3data| | 
+---------------------------+ 

**User Guide's Index**

.. toctree::

   overview
   first_steps
   how_to_connect
   data_transfer
   storage
   system_use
   parallelization
   batch_system
   cron
   job_signals
   compilers_and_dev_tools
   env_modules
   scratch_dirs
   fat_nodes
   gpu_nodes
   other_nodes
   FAQ
   want_to_know_more

