.. _ft3_batch_job_array:

Job array
---------
A job array in Slurm implies that the same script is going to be executed “n” times, the only difference between each execution is the environment variable, *$SLURM_ARRAY_TASK_ID*.

This option simplifies the sending of “n” similar jobs and provides the user the management of an unique job but internally, SLURMS treats them as “n” independent jobs and thus they contribute to all user limits (partition and QoS).

**SBATCH option:** ``-a``, ``--array=<indexes>`` 

- The indexes specification identifies the indexes values. Multiple values may be specified using a comma separated list and/or a range of values with a “-” separator, for example: ``--array=0-15`` or ``--array=0,6,16-32``. 

- A step function can also be specified with a suffix containing ":" and a number, for example, ``--array=0-15:4`` is equivalent to ``--array=0,4,8,12``. 

- A maximum number of simultaneously running tasks from the job array may be specified using a “%” separator. For example ``--array=0-15%4`` will limit the number of simultaneously running tasks from this job array to 4. The minimum index value is 0 and the maximum value is MaxArraySize -1.

**INPUT environment variable:**

- SBATCH_ARRAY_INX: Same as -a, --array=<indexes>

**OUTPUT environment variables:**

- SLURM_ARRAY_TASK_ID : Job array ID (index) number.
- SLURM_ARRAY_JOB_ID : Job array’s master job ID number.
- SLURM_ARRAY_TASK_COUNT: Total number of tasks in a job array.
- SLURM_ARRAY_TASK_MAX: Job array’s maximum ID (index) number.
- SLURM_ARRAY_TASK_MIN: Job array’s minimum ID (index) number.
- SLURM_ARRAY_TASK_STEP: Job array’s index step size. 

.. code-block::
    
    #!/bin/bash
    #SBATCH --job-name=test_job_arr
    #SBATCH --output=res_job_arr.txt
    #SBATCH --ntasks=1
    #SBATCH --time=00:10:00
    #SBATCH --mem-per-cpu=1G
    #SBATCH --array=1-8
    srun ./program.exe $SLURM_ARRAY_TASK_ID

Specified by the --array parameter, the command will be run eight times, creating eight distinct jobs, each time with a different argument passed with the environment variable defined by slurm SLURM_ARRAY_TASK_ID ranging from 1 to 8.

.. code-block::
    
    #!/bin/bash
    #SBATCH --job-name=test_job_arr
    #SBATCH --output=res_job_arr.txt
    #SBATCH --ntasks=1
    #SBATCH --time=00:10:00
    #SBATCH --mem-per-cpu=1G
    #SBATCH --array=0-7
    FILES=(/path/to/data/*)
    srun ./program.exe ${FILES[$SLURM_ARRAY_TASK_ID]}

This process can be used to pass 8 different files (located in the indicated path) to read based upon de value of the variable $SLURM_*. 

There are more examples at FinisTerrae III path: /opt/cesga/job-scripts-examples-ft3/Job_Array.sh
