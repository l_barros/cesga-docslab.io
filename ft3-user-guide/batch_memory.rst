.. _ft3_memory:

Memory and time
==============

.. warning:: Both are mandatory parameter in job submission.

Memory demand for the job must be specified with the options: ``--mem`` or ``--mem-per-cpu``.
    - Input environment variable: **SBATCH_MEM_PER_NODE**
    - Input environment variable: **SBATCH_MEM_PER_CPU**

Maximun execution time must be set using: ``--time=D-HH:MM:SS`` (or in reduced form ``-t D-HH:MM:SS``)
    - Input environment variable: **SBATCH_TIMELIMIT**
Examples:

- 1 task job using 1 core requesting 6GB of total memory and 1 hour execution::

    $ sbatch -n 1 --mem=6GB script.sh --time=01:00:00

- 1 task job using 2 core requesting 40GB of total memory (20 GB per core) and 1 hour execution::

    $ sbatch -n 1 --cpus-per-task=2 --mem-per-cpu=20GB script.sh -t 01:00:00
