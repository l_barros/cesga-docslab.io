.. _ft3_partitions:

Partitions
==========

From the **431 nodes** of the cluster, **354** are original for FinisTerrae III and the remaining 74 were recovered from previous clusters of our infraestructure. All of them are shared between the following partitions:

- **short, medium, long, ondemand and requeue partitions:** share the bulk of nodes (336 nodes) encompassing the following groups of nodes:
    - **ilk**: 256 nodes with 64 cores and 246GB of RAM available. 
    - **smp**: 16 nodes with 64 cores and 2011GB of RAM available. 
    - **a100**: 64 nodes with 64 cores, 246GB of RAM available and 2 GPU A100.
    - **clk**: 74 nodes with 48 cores, 180GB of RAM available.
- **viz partition:** 16 nodes with 64 cores and 246GB of RAM available and 1 GPU T4 devoted to interactive use.
- **data partition:** 2 nodes with 64 cores and 246GB of RAM available devoted to data transfer.

.. note:: Automatically partition asigment will be done on short, medium, long, ondemand and requeue partitions depending on demanded resources and job characteristics.

To see more information about partitions, you can use the ``batchlim`` on the command line and it will show you partition limits, QOS limits and your user and account limits.

.. code-block:: console
 
 Name      | Default |  MaxNodes |     MaxTime | Shared | TotalCPUs | TotalNodes | MaxMemPerNode
 ----------|---------|-----------|-------------|--------|-----------|------------|--------------
 ondemand  | NO      | UNLIMITED | 42-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
 long      | NO      | UNLIMITED |  7-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
 medium    | NO      | UNLIMITED |  3-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
 requeue   | NO      | UNLIMITED |  1-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
 short     | YES     | UNLIMITED |    06:00:00 | NO     |     21504 |        336 | UNLIMITED MB
 viz       | NO      | UNLIMITED |    08:00:00 | NO     |       640 |         10 | UNLIMITED MB
 data      | NO      | UNLIMITED |  2-00:00:00 | NO     |       128 |          2 | UNLIMITED MB

