.. _ft3_batch_stdout_err:

Standart output and error
=========================

By default, slurm combines standard and error output into a single file 
called **slurm-<job_number>.out** or 
**slurm-<job_number>_<array_index>.out** if it's a job array. By default 
these files are saved in the submission directory. This behavior can be 
modified with the following options of the ``sbatch`` command:

**\-\-error /path/to/dir/filename**

**\-\-output /path/to/dir/filename**

